function perform()
{
    let str:HTMLInputElement = <HTMLInputElement>document.getElementById("str");
    let ans:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("ans");

    var s:string = str.value;
    if (!isNaN(parseFloat(s)))
    {
        ans.innerHTML = "";
        alert("Please enter a string.");
    }
    else
    {
        var uc:string = s.toUpperCase();
        var lc:string = s.toLowerCase();
        var sp:string[] = s.split(" ");
        ans.innerHTML = "The string in uppercase is: " + uc + ".<br><br>The string in lowercase is: " + lc + ".<br><br>The string split at spaces is: " + sp + ".";
    }
}