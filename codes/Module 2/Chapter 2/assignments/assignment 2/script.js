function perform() {
    let str = document.getElementById("str");
    let ans = document.getElementById("ans");
    var s = str.value;
    if (!isNaN(parseFloat(s))) {
        ans.innerHTML = "";
        alert("Please enter a string.");
    }
    else {
        var uc = s.toUpperCase();
        var lc = s.toLowerCase();
        var sp = s.split(" ");
        ans.innerHTML = "The string in uppercase is: " + uc + ".<br><br>The string in lowercase is: " + lc + ".<br><br>The string split at spaces is: " + sp + ".";
    }
}
//# sourceMappingURL=script.js.map