function perform()
{
    let str:HTMLInputElement = <HTMLInputElement>document.getElementById("str");
    let ans:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("ans");

    var s:string = str.value;
    if (!isNaN(parseFloat(s)))
    {
        ans.innerHTML = "";
        alert("Please enter a string.");
    }
    else
    {
        if (s.includes("SCRIPT"))
        {
            var ind:number = s.indexOf("SCRIPT");
            var index:number = ind;
            while (s[index] != "T" && index < s.length)
            {
                index++;
            }
            ans.innerHTML = "Displaying substring: " + s.substring(ind, index + 1) + ".<br>";
            if (s.includes("E"))
            {
                ans.innerHTML += "Character 'E' is at position " + s.indexOf("E") + ".<br>";
            }
            else
            {
                ans.innerHTML += "There is no occurence of the character 'E' in the given string.<br>";
            }
        }
        else if (s.includes("E"))
        {
            if (s.includes("SCRIPT"))
            {
                var ind:number = s.indexOf("SCRIPT");
                var index:number = ind;
                while (s[index] != "T" && index < s.length)
                {
                    index++;
                }
                ans.innerHTML = "Displaying substring: " + s.substring(ind, index + 1) + ".<br>";   
                ans.innerHTML += "Character 'E' is at position " + s.indexOf("E") + ".<br>";
            }
            else
            {
                ans.innerHTML = "There is no substring 'SCRIPT' in the given string. <br> Character 'E' is at position " + s.indexOf("E") + ".<br>";
            }
        }
        else
        {
            ans.innerHTML = "There is neither a substring 'SCRIPT' nor the character 'E' in the given string.<br>.";
        }
    }
}