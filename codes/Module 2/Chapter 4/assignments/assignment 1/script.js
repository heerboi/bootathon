function start() {
    let t1 = document.getElementById("11");
    var num = parseInt(t1.value);
    console.log(num);
    let p1 = document.getElementById("pos");
    let p2 = document.getElementById("neg");
    let p3 = document.getElementById("zero");
    if (isNaN(num) || num <= 0) {
        alert("Please enter a number greater than zero.");
    }
    else {
        var pos = 0, neg = 0, zero = 0;
        for (var a = 1; a <= num; a++) {
            var nums = prompt("Enter number " + a.toString() + ": ");
            var num1 = parseInt(nums);
            if (nums == null) {
                break;
            }
            else if (isNaN(num1)) {
                alert("Please enter a number and try again.");
            }
            else {
                if (num1 > 0) {
                    pos++;
                }
                else if (num1 == 0) {
                    zero++;
                }
                else {
                    neg++;
                }
            }
        }
        p1.innerHTML = "Positive numbers: " + pos.toString();
        p2.innerHTML = "Negative numbers: " + neg.toString();
        p3.innerHTML = "Zeroes: " + zero.toString();
    }
}
//# sourceMappingURL=script.js.map