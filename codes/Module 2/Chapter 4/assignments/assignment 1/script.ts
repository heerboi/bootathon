function start()
{
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("11");
    var num:number = parseInt(t1.value);
    console.log(num);
    let p1:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("pos");
    let p2:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("neg");
    let p3:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("zero");
    if (isNaN(num) || num <= 0)
    {
        alert("Please enter a number greater than zero.");
    }
    else
    {
        var pos:number = 0, neg:number = 0, zero:number = 0;
        for (var a:number = 1; a <= num; a++)
        {
            var nums:string = prompt("Enter number " + a.toString() + ": ");
            var num1:number = parseInt(nums);
            if (nums == null)
            {
                break;
            }
            else if (isNaN(num1))
            {
                alert("Please enter a number and try again.");
            }
            else {
                if (num1 > 0)
                {
                    pos++;

                }
                else if (num1 == 0)
                {
                    zero++;
                }
                else
                {
                    neg++;
                }
            }
        }
        p1.innerHTML = "Positive numbers: " + pos.toString();
        p2.innerHTML = "Negative numbers: " + neg.toString();
        p3.innerHTML = "Zeroes: " + zero.toString();
    }
}