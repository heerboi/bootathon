/*Write a program to create dynamic table generation for multiplication table of any number and table should be from 1 to entered number. For example :
When user will enter number as 6 then table will start from 6 * 1 = 6 and it will end at 6 * 6 = 36.*/
function gen() {
    let inp = document.getElementById("inp");
    let tab = document.getElementById("tab");
    var val = parseInt(inp.value);
    while (tab.rows.length > 1) {
        tab.deleteRow(1);
    }
    if (isNaN(val) || val <= 0) {
        alert("Please enter a number greater than 1.");
    }
    else {
        var a;
        for (a = 1; a <= val; a++) {
            var row = tab.insertRow();
            var cell = row.insertCell();
            var text1 = document.createElement("input");
            text1.type = "text";
            text1.style.textAlign = "center";
            text1.value = val.toString();
            text1.readOnly = true;
            text1.style.border = "none";
            text1.style.fontSize = "20px";
            cell.appendChild(text1);
            var text2 = document.createElement("input");
            text2.type = "text";
            text2.style.textAlign = "center";
            text2.value = a.toString();
            text2.readOnly = true;
            text2.style.border = "none";
            text2.style.fontSize = "20px";
            var cell = row.insertCell();
            cell.appendChild(text2);
            var text3 = document.createElement("input");
            text3.type = "text";
            text3.style.textAlign = "center";
            text3.value = (val * a).toString();
            text3.readOnly = true;
            text3.style.border = "none";
            text3.style.fontSize = "20px";
            var cell = row.insertCell();
            cell.appendChild(text3);
            /*var text:HTMLInputElement = document.createElement("input");
            text.type = "text";
            text.style.textAlign = "center";
            text.value = val.toString();
            cell.appendChild(text);
    
            var cell:HTMLTableDataCellElement = row.insertCell();
    
            var text:HTMLInputElement = document.createElement("input");
            text.type = "text";
            text.style.textAlign = "center";
            text.value = (val*val).toString();
            cell.appendChild(text);*/
        }
    }
}
//# sourceMappingURL=script.js.map