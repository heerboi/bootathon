function find() {
    let inp = document.getElementById("inp");
    let real = document.getElementById("real");
    let img = document.getElementById("img");
    var st = inp.value;
    var r = parseFloat(st);
    if (st.includes("+")) {
        var items = st.split("+");
        var r;
        var im;
        var i;
        if (items[0].includes("i") && !items[1].match(/[A-Za-z]/)) {
            r = parseFloat(items[1]);
            i = items[0].split("i");
            im = parseInt(i[0]);
            real.innerHTML = "The real part is: " + r.toString();
            img.innerHTML = "The imaginary part is: " + im.toString();
            if (isNaN(im) || i[1].match(/[a-zA-Z0-9]/)) {
                real.innerHTML = "";
                img.innerHTML = "";
                alert("Please enter the number in the form a + bi.");
            }
            else if (isNaN(r)) {
                real.innerHTML = "";
                img.innerHTML = "";
                alert("Please enter a valid number.");
            }
        }
        else if (items[1].includes("i") && !items[0].match(/[A-Za-z]/)) {
            r = parseFloat(items[0]);
            i = items[1].split("i");
            im = parseInt(i[0]);
            real.innerHTML = "The real part is: " + r.toString();
            img.innerHTML = "The imaginary part is: " + im.toString();
            if (isNaN(im) || i[1].match(/[a-zA-Z0-9]/)) {
                real.innerHTML = "";
                img.innerHTML = "";
                alert("Please enter the number in the form a + bi.");
            }
            else if (isNaN(r)) {
                real.innerHTML = "";
                img.innerHTML = "";
                alert("Please enter a valid number.");
            }
        }
        else {
            real.innerHTML = "";
            img.innerHTML = "";
            alert("Please enter the number in the form a + bi.");
        }
    }
    else if (st.includes("i") && st.split("i").length == 2 && st.split("i")[1].length == 0 && (st.split("i")[0].length == 1 || st.split("i")[0].length == 2)) {
        var imag = st.split("i");
        if (!isNaN(parseFloat(imag[0]))) {
            var imagi = parseFloat(imag[0]);
            real.innerHTML = "The real part is: 0";
            img.innerHTML = "The imaginary part is: " + imagi.toString();
        }
        else {
            real.innerHTML = "";
            img.innerHTML = "";
            alert("Please enter a complex number in the form bi.");
        }
    }
    else if (st.includes("-")) {
        var items;
        var a = 0;
        if (st.startsWith("-")) {
            items = st.slice(1, st.length).split("-");
            a = 1;
        }
        else {
            items = st.split("-");
        }
        var r;
        var im;
        var i;
        if (items.length > 1) {
            if (items[0].includes("i") && !items[1].match(/[A-Za-z]/)) {
                r = parseFloat(items[1]);
                i = items[0].split("i");
                im = parseInt(i[0]);
                real.innerHTML = "The real part is: -" + r.toString();
                if (a == 0) {
                    img.innerHTML = "The imaginary part is: " + im.toString();
                }
                else {
                    img.innerHTML = "The imaginary part is: -" + im.toString();
                }
                if (isNaN(im) || i[1].match(/[a-zA-Z0-9]/)) {
                    real.innerHTML = "";
                    img.innerHTML = "";
                    alert("Please enter the number in the form a + bi.");
                }
                else if (isNaN(r)) {
                    real.innerHTML = "";
                    img.innerHTML = "";
                    alert("Please enter a valid number.");
                }
            }
            else if (items[1].includes("i") && !items[0].match(/[A-Za-z]/)) {
                r = parseFloat(items[0]);
                i = items[1].split("i");
                im = parseInt(i[0]);
                if (a == 0) {
                    real.innerHTML = "The real part is: " + r.toString();
                }
                else {
                    real.innerHTML = "The real part is: -" + r.toString();
                }
                img.innerHTML = "The imaginary part is: -" + im.toString();
                if (isNaN(im) || i[1].match(/[a-zA-Z0-9]/)) {
                    real.innerHTML = "";
                    img.innerHTML = "";
                    alert("Please enter the number in the form a + bi.");
                }
                else if (isNaN(r)) {
                    real.innerHTML = "";
                    img.innerHTML = "";
                    alert("Please enter a valid number.");
                }
            }
            else {
                real.innerHTML = "";
                img.innerHTML = "";
                alert("Please enter the number in the form a + bi.");
            }
        }
        else {
            real.innerHTML = "";
            img.innerHTML = "";
            alert("Please enter the number in the form a + bi.");
        }
    }
    else if (!isNaN(r) && !st.match(/[A-Za-z]/)) {
        real.innerHTML = "The real part is: " + r.toString();
        img.innerHTML = "The imaginary part is: 0";
    }
    else {
        real.innerHTML = "";
        img.innerHTML = "";
        alert("Please enter a valid constant or a complex number in the form a + ib.");
    }
}
//# sourceMappingURL=script.js.map