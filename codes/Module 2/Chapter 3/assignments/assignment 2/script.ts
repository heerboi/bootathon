function tri()
{
    let t1:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("t1");
    let s1:HTMLInputElement = <HTMLInputElement>document.getElementById("s1");
    let s2:HTMLInputElement = <HTMLInputElement>document.getElementById("s2");
    let s3:HTMLInputElement = <HTMLInputElement>document.getElementById("s3");
    var n1:number = parseFloat(s1.value);
    var n2:number = parseFloat(s2.value);
    var n3:number = parseFloat(s3.value);
    if (isNaN(n1) || isNaN(n2) || isNaN(n3) || n1 <= 0 || n2 <= 0 || n3 <= 0)
    {
        alert("Please enter numeric values greater than zero.")
        t1.innerHTML = "";
    }
    else
    {
        if (n1 == n2 && n2 == n3)
        {
            t1.innerHTML = "The triangle is an equilateral triangle.<br>";
        }
        else if (((n1 == n2) && n1 != n3) || ((n2 == n3) && n2 != n1) || ((n3 == n1) && n3 != n2))
        {
            t1.innerHTML = "The triangle is an isosceles triangle.<br>";
            if ((Math.pow(n1, 2) + Math.pow(n2, 2) == Math.pow(n3, 2)) || (Math.pow(n1, 2) + Math.pow(n2, 2) == Math.pow(n3, 2)) || (Math.pow(n1, 2) + Math.pow(n2, 2) == Math.pow(n3, 2)))
            {
                t1.innerHTML += "The triangle is also a right angled triangle.";
            }
        }
        else
        {
            t1.innerHTML = "The triangle is a scalene triangle.<br>";
            if ((Math.pow(n1, 2) + Math.pow(n2, 2) == Math.pow(n3, 2)) || (Math.pow(n1, 2) + Math.pow(n2, 2) == Math.pow(n3, 2)) || (Math.pow(n1, 2) + Math.pow(n2, 2) == Math.pow(n3, 2)))
            {
                t1.innerHTML += "The triangle is also a right angled triangle.";
            }
        }
    }
}