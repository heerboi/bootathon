function check()
{
    let x1:HTMLInputElement = <HTMLInputElement>document.getElementById("x1");
    let y1:HTMLInputElement = <HTMLInputElement>document.getElementById("y1");
    let x2:HTMLInputElement = <HTMLInputElement>document.getElementById("x2");
    let y2:HTMLInputElement = <HTMLInputElement>document.getElementById("y2");
    let x3:HTMLInputElement = <HTMLInputElement>document.getElementById("x3");
    let y3:HTMLInputElement = <HTMLInputElement>document.getElementById("y3");
    let x:HTMLInputElement = <HTMLInputElement>document.getElementById("x");
    let y:HTMLInputElement = <HTMLInputElement>document.getElementById("y");
    let ans:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("ans");

    var m1:number = parseFloat(x1.value);
    var n1:number = parseFloat(y1.value);
    var m2:number = parseFloat(x2.value);
    var n2:number = parseFloat(y2.value);
    var m3:number = parseFloat(x3.value);
    var n3:number = parseFloat(y3.value);
    var m:number = parseFloat(x.value);
    var n:number = parseFloat(y.value);

    if (isNaN(m1) || isNaN(n1) || isNaN(m2) || isNaN(n2) || isNaN(m3) || isNaN(n3) || isNaN(m) || isNaN(n))
    {
        ans.innerHTML = "";
        alert("Please enter numeric co-ordinates of the triangle vertices.");
    }
    else
    {
        var abc:number = Math.abs(1/2 * ((m1 * (n2 - n3)) + (m2 * (n3 - n1)) + (m3 * (n1 - n2))));
        var pbc:number = Math.abs(1/2 * ((m * (n2- n3)) + (m2 * (n3 - n)) + (m3 * (n - n2))));
        var pab:number = Math.abs(1/2 * ((m * (n1 - n2)) + (m1 * (n2 - n)) + (m2 * (n - n1))));
        var pac:number = Math.abs(1/2 * ((m * (n1 - n3)) + (m1 * (n3 - n)) + (m3 * (n - n1))));
        if (Math.abs((pab + pac + pbc) - abc) <= 0.000001)
        {
            ans.innerHTML = "The point is inside the triangle.";
        }
        else
        {
            ans.innerHTML = "The point is not inside the triangle.";
        }
    }
}