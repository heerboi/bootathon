function find()
{
    let ans:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("ans");
    let inp:HTMLInputElement = <HTMLInputElement>document.getElementById("inp");
    var val:number = parseInt(inp.value);
    if (isNaN(val))
    {
        alert("Please enter a number greater than 1.");
        ans.innerHTML = "";
    }
    else
    {
        if (val % 2 == 0)
        {
            ans.innerHTML = "The number is even.";
        }
        else
        {
            ans.innerHTML = "The number is odd.";
        }
    }
}