function find() {
    let ans = document.getElementById("ans");
    let inp = document.getElementById("inp");
    var val = parseInt(inp.value);
    if (isNaN(val)) {
        alert("Please enter a number greater than 1.");
        ans.innerHTML = "";
    }
    else {
        if (val % 2 == 0) {
            ans.innerHTML = "The number is even.";
        }
        else {
            ans.innerHTML = "The number is odd.";
        }
    }
}
//# sourceMappingURL=script.js.map