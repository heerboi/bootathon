let ip1 : HTMLInputElement = <HTMLInputElement>document.getElementById("num1");
let ip2 : HTMLInputElement = <HTMLInputElement>document.getElementById("num2");
let ans : HTMLInputElement = <HTMLInputElement>document.getElementById("ans");
let sel : HTMLSelectElement = <HTMLSelectElement>document.getElementById("sel");


function add()
{
    if (isNaN(parseFloat(ip1.value)) || isNaN(parseFloat(ip2.value)))
    {
        ans.value = "";
        alert("Please enter a number.")
    } 
    else
    {   
        var c:number = parseFloat(ip1.value) + parseFloat(ip2.value);
        ans.value = c.toString();
    }
}

function sub()
{
    if (isNaN(parseFloat(ip1.value)) || isNaN(parseFloat(ip2.value)))
    {
        ans.value = "";
        alert("Please enter a number.")
    } 
    else
    {
        var c:number = parseFloat(ip1.value) - parseFloat(ip2.value);
        ans.value = c.toString();
    }
}


function mul()
{
    if (isNaN(parseFloat(ip1.value)) || isNaN(parseFloat(ip2.value)))
    {
        ans.value = "";
        alert("Please enter a number.")
    }
    else
    { 
        var c:number = parseFloat(ip1.value) * parseFloat(ip2.value);
        ans.value = c.toString();
    }
}

function div()
{
    if (isNaN(parseFloat(ip1.value)) || isNaN(parseFloat(ip2.value)))
    {
        ans.value = "";
        alert("Please enter a number.")
    }
    else 
    {
        var c:number = parseFloat(ip1.value) / parseFloat(ip2.value);
        ans.value = c.toString();
    }
}

function sin()
{
    if (isNaN(parseFloat(ip1.value)))
    {
        ans.value = "";
        alert("Please enter a number.")
    }
    else if ((ip2.value != "" && ip1.value == "") || (ip2.value != "" && ip1.value != ""))
    {
        ans.value = "";
        alert("Please enter the number in input 1 and leave input 2 blank.");
    }
    else
    {
        var ip:number = parseFloat(ip1.value);
        var value:number;
        if (sel.value == "deg")
        {
            value = Math.sin(ip * (Math.PI / 180));
        }
        else
        {
            value = Math.sin(ip);
        }
        ans.value = value.toString();
    }
}

function cos()
{
    if (isNaN(parseFloat(ip1.value)))
    {
        ans.value = "";
        alert("Please enter a number.")
    }
    else if ((ip2.value != "" && ip1.value == "") || (ip2.value != "" && ip1.value != ""))
    {
        ans.value = "";
        alert("Please enter the number in input 1 and leave input 2 blank.");
    }
    else
    {
        var ip:number = parseFloat(ip1.value);
        var value:number;
        if (sel.value == "deg")
        {
            value = Math.cos(ip * (Math.PI / 180));
        }
        else
        {
            value = Math.cos(ip);
        }
        ans.value = value.toString();
    }
}

function tan()
{
    if (isNaN(parseFloat(ip1.value)))
    {
        ans.value = "";
        alert("Please enter a number.")
    }
    else if ((ip2.value != "" && ip1.value == "") || (ip2.value != "" && ip1.value != ""))
    {
        ans.value = "";
        alert("Please enter the number in input 1 and leave input 2 blank.");
    }
    else
    {
        var ip:number = parseFloat(ip1.value);
        var value:number;
        if (sel.value == "deg")
        {
            value = Math.tan(ip * (Math.PI / 180));
        }
        else
        {
            value = Math.tan(ip);
        }
        ans.value = value.toString();
    }
}

function sqrt()
{
    if (isNaN(parseFloat(ip1.value)) || isNaN(parseFloat(ip2.value)))
    {
        ans.value = "";
        alert("Please enter a number.")
    }
    else if ((ip2.value != "" && ip1.value == "") || (ip2.value != "" && ip1.value != ""))
    {
        ans.value = "";
        alert("Please enter the number in input 1 and leave input 2 blank.");
    }
    else
    {
        var ip:number = parseFloat(ip1.value);
        var value:number = Math.sqrt(ip);
        ans.value = value.toString();
    }
}

function power()
{
    if (isNaN(parseFloat(ip1.value)) || isNaN(parseFloat(ip2.value)))
    {
        ans.value = "";
        alert("Please enter a number.")
    }
    else 
    {
        var c:number = Math.pow(parseFloat(ip1.value), parseFloat(ip2.value));
        ans.value = c.toString();
    }
}