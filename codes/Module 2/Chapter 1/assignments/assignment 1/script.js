function com() {
    let rad = document.getElementById("rad");
    let ans = document.getElementById("ans");
    var radius = parseFloat(rad.value);
    if (isNaN(radius) || radius < 0) {
        ans.innerHTML = "";
        alert("Please enter a positive numeric radius.");
    }
    else {
        var area = Math.PI * Math.pow(radius, 2);
        ans.innerHTML = "The area of a circle with radius " + radius.toString() + " is " + area.toString() + ".<br>";
    }
}
//# sourceMappingURL=script.js.map