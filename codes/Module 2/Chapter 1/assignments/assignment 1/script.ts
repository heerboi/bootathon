function com()
{
    let rad:HTMLInputElement = <HTMLInputElement>document.getElementById("rad");
    let ans:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("ans");
    var radius:number = parseFloat(rad.value);
    if (isNaN(radius) || radius < 0)
    {
        ans.innerHTML = "";
        alert("Please enter a positive numeric radius.");
    }
    else
    {
        var area:number = Math.PI * Math.pow(radius, 2);
        ans.innerHTML = "The area of a circle with radius " + radius.toString() + " is " + area.toString() + ".<br>";
    }
}