function calculate() {
    let x1 = document.getElementById("x1");
    let y1 = document.getElementById("y1");
    let x2 = document.getElementById("x2");
    let y2 = document.getElementById("y2");
    let x3 = document.getElementById("x3");
    let y3 = document.getElementById("y3");
    let ans = document.getElementById("ans");
    var m1 = parseFloat(x1.value);
    var n1 = parseFloat(y1.value);
    var m2 = parseFloat(x2.value);
    var n2 = parseFloat(y2.value);
    var m3 = parseFloat(x3.value);
    var n3 = parseFloat(y3.value);
    if (isNaN(m1) || isNaN(n1) || isNaN(m2) || isNaN(n2) || isNaN(m3) || isNaN(n3)) {
        ans.innerHTML = "";
        alert("Please enter numeric co-ordinates of the triangle vertices.");
    }
    else {
        var area = Math.abs(1 / 2 * ((m1 * (n2 - n3)) + (m2 * (n3 - n1)) + (m3 * (n1 - n2))));
        ans.innerHTML = "The area of the triangle with the following vertices is " + area.toString() + ".<hr><br>";
    }
}
//# sourceMappingURL=script.js.map