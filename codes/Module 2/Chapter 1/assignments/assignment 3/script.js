function calculate() {
    let sel = document.getElementById("sel");
    let angle = document.getElementById("angle");
    let ans = document.getElementById("ans");
    var choice = sel.value;
    var ang = parseFloat(angle.value);
    if (isNaN(ang)) {
        ans.innerHTML = "";
        alert("Please enter a valid angle.");
    }
    else {
        if (choice = "deg") {
            var value = ang + Math.cos(ang * (Math.PI / 180));
            ans.innerHTML = "The value of <i>x + cos(x)</i> is " + value.toString() + ".<br>";
        }
        else {
            var value = ang + Math.cos(ang);
            ans.innerHTML = "The value of <i>x + cos(x)</i> is " + value.toString() + ".<br>";
        }
    }
}
//# sourceMappingURL=script.js.map