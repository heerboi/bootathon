function calculate()
{
    let sel:HTMLSelectElement = <HTMLSelectElement>document.getElementById("sel");
    let angle:HTMLInputElement = <HTMLInputElement>document.getElementById("angle");
    let ans:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("ans");
    
    var choice = sel.value;
    var ang:number = parseFloat(angle.value);
    if (isNaN(ang))
    {
        ans.innerHTML = "";
        alert("Please enter a valid angle.");
    }
    else
    {
        if (choice = "deg")
        {
            var value:number = ang + Math.cos(ang * (Math.PI / 180));
            ans.innerHTML = "The value of <i>x + cos(x)</i> is " + value.toString() + ".<br>";
        }
        else
        {
            var value:number = ang + Math.cos(ang);
            ans.innerHTML = "The value of <i>x + cos(x)</i> is " + value.toString() + ".<br>";
        }
    }
}