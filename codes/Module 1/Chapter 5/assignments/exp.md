<h6 style = "text-align: center;"><b>Fundamentals of Computer Networks</b></h6>
<h1 style = "text-align: center; font-size: 50px;">IPv4 Addressing</h1>
<hr style = "height: 2px; background-color: crimson;">
<p><span style = "font-size: 20px;"><b><u>Aim :</u></b></span></p>
<p style = "font-size:15px;">To give <b><i>IP address</i></b> of different classes in given Network ID.<br><br>
The goal of the experiment is to make user learn the concept of <b><i>IP addressing</i></b> and let them manually set up IP address in the give network and check for the same.
<hr style = "height: 2px; background-color: crimson;">
<p><span style = "font-size: 20px;"><b><u>Theory :</u></b></span></p>
<p style = "font-size:15px;">IP addresses enable computers to communicate by providing unique identifiers for the computer itself and for the network over which it is located. An <b><i>IP address</i></b> is a <i><u>32 bit value that contains a network identifier(net - id) and a host identifier(host - id)</u></i>.<br>
The network administrators need to assign IP addresses to the system on their network. This address needs to be a unique one. All the computers on a particular subnet will have the same network identifier but different host identifiers. <b><i>The Internet Assigned Numbers Authority (IANA)</i></b> assigns network identifiers to avoid any duplication of addresses.<br>
Host Identifier Network identifies 32 bits.
The <i>32 bit IPv4 address</i> is <i><u>grouped into groups of eight bits, separated by dots</u></i>. Each <i>8 bit group</i> is then <i><u>converted into its equivalent binary number</u></i>. Thus each <i>octet (8 bit)</i> can take value from <u>0 to 255</u>. The IPv4 in the dotted decimal notation can range from <u>0.0.0.0 to 255.255.255.255</u>.<br>
IPv4 Address are classified into <u><i>5 types</i></u> as follows:<br></p>

1. __Class A__
2. __Class B__
3. __Class C__
4. __Class D__
5. __Class E__

<hr style = "height: 2px; background-color: crimson;">

## __<u>1. Class A :</u>__
<p style = "font-size:15px; margin-left: 15px;">The <u>first bit of the first octet</u> is always set to <u>0(zero)</u>. Thus the first octet ranges from <u>1-127</u> i.e.
<br><b>00000000 - 01111111<br>
1 - 127</b><br>
Class A addresses only include IP starting from <u>1.x.x.x to 126.x.x.x only</u>.<br> The IP range <u>127.x.x.x is reserved for loopback IP addresses</u>.<br> The default subnet mask for class Class A IP address is <u>255.0.0.0</u> which implies that Class A addressing can have <u>126 networks and 167,777,214 hosts</u>.<br> Class A IP address format is thus : <b>0NNNNNNN.HHHHHHHH.HHHHHHHH.HHHHHHHH</b>.</p>
<hr style = "height: 2px; background-color: crimson;">

## __<u>1. Class B :</u>__
<p style = "font-size:15px; margin-left: 15px;">An IP address which belongs to class B has the <u>first two bits</u> in the <u>first octet</u> set to <u>10</u>, i.e.<br>
<b>10000000 - 10111111<br>
128 - 191</b><br>
Class B IP Addresses range from <u>128.0.x.x to 191.255.x.x</u>.<br> The default subnet mask for Class B is <u>255.255.x.x</u>.<br> Class B has <u>16,384 Network addresses and 65534 host addresses</u>. <br>Class B IP addresses format is: <b>10NNNNNN.NNNNNNNN.HHHHHHHH.HHHHHHHH</b>.</p>
<hr style = "height: 2px; background-color: crimson;">

## __<u>1. Class C :</u>__
 <p style = "font-size:15px; margin-left: 15px;">The <u>first octet</u> of Class C IP address has its <u>first 3 bits</u> set to <u>110</u>, that is:<br>
 <b>11000000 - 11011111<br>
 192 - 223</b><br>
 Class C IP addresses range from <u>192.0.0.x to 223.255.255.x</u>.<br> The default subnet mask for Class C is <u>255.255.255.x</u>.<br> Class C gives <u>2,097,152 Network addresses and 254 Host addresses</u>. <br>Class C IP address format is : <b>110NNNNN.NNNNNNNN.NNNNNNNN.HHHHHHHH</b>.
 <hr style = "height: 2px; background-color: crimson;">

 <p><span style = "font-size: 20px;"><b><u>Procedure :</u></b></span></p>
 <ol type = "1" style = "font-size:15px;">
 <li>The aim is to give IP addresses to the PCs.</li>
 <li>To perform the experiment follow the below steps.</li>
 <li>A choice list would be given defining the classes.</li>
 <li>The user has to select the class in which they choose to give IP addresses.</li>
 <li>After that a Network ID would be given and the user has to enter the IP addresses according to the Network ID.</li>
 <li>Click on submit to test whether the IP address given to PCs make them into network or not.</li>
 </ol>

 <hr style = "height: 2px; background-color: crimson;">