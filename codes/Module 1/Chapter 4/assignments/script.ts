/**
 * TypeScript file for the logic implemented in calculator.html
 * By Heer Patel
 */


 /**
  * Here, we initialize variables of type 'HTMLInputElement' to the values obtained from the input elements in our html
  * webpage.
  */
var ip1 : HTMLInputElement = <HTMLInputElement>document.getElementById("num1");
var ip2 : HTMLInputElement = <HTMLInputElement>document.getElementById("num2");
var ans : HTMLInputElement = <HTMLInputElement>document.getElementById("ans");


/**
 * This is the add() function, which, when called, stores the addition of the two inputs as the value of the third input element.
 */
function add()
{
    var c:number = parseFloat(ip1.value) + parseFloat(ip2.value);
    ans.value = c.toString();
}


/**
 * This is the sub() which performs the subtraction of the two numbers and stores it as the value of the third input element.
 */
function sub()
{
    var c:number = parseFloat(ip1.value) - parseFloat(ip2.value);
    ans.value = c.toString();
}


/**
 * This is the mul() which performs the multiplication of the two numbers and stores it as the value of the third input element.
 */
function mul()
{
    var c:number = parseFloat(ip1.value) * parseFloat(ip2.value);
    ans.value = c.toString();
}


/**
 * This is the div() which performs the division of the two numbers and stores it as the value of the third input element.
 */
function div()
{
    var c:number = parseFloat(ip1.value) / parseFloat(ip2.value);
    ans.value = c.toString();
}